/* eslint-disable linebreak-style */
import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import colors from '../styles/colors';
import Beranda from '../screens/Beranda';
import KalenderAkademik from '../screens/KalenderAkademik';
import Senin from '../screens/Senin';
import Selasa from '../screens/Selasa';
import Rabu from '../screens/Rabu';
import Kamis from '../screens/Kamis';
import SplashScreen from '../screens/SplashScreen';
import Iicon from 'react-native-vector-icons/Ionicons';

// Membuat Stack dan Bottom Kedalam Variabel
const Stack = createStackNavigator();
const BottomStack = createBottomTabNavigator();

const BottomRouter = () => {
  return (
    <BottomStack.Navigator
      initialRouteName='Beranda'
      tabBarOptions={{
        activeTintColor: colors.third,
      }}
    >

      <BottomStack.Screen
        name='Beranda'
        component={Beranda}
        options={{
          tabBarLabel: 'Menu Utama',
          tabBarIcon: ({color}) => (
            <Iicon name='home' size={27} color={color} />
          ),
        }}
      />
      <BottomStack.Screen
        name="Kalender"
        component={KalenderAkademik}
        options={{
          tabBarLabel: 'Kalender',
          tabBarIcon: ({color}) => (
            <Iicon name="today-sharp" size={27} color={color} />
          ),
        }}
      />
      <BottomStack.Screen
        name="senin"
        component={Senin}
        options={{
          tabBarLabel: 'Jadwal',
          tabBarIcon: ({color}) => (
            <Iicon name="library-outline" size={27} color={color} />
          ),
        }}
      />

    </BottomStack.Navigator>
  );
};

const Router = () => {
  return (
    <Stack.Navigator initialRouteName="SplashScreen" >

      <Stack.Screen
        name="Beranda"
        component={BottomRouter}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="Kalender"
        component={KalenderAkademik}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="senin"
        component={Senin}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="selasa"
        component={Selasa}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="rabu"
        component={Rabu}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="kamis"
        component={Kamis}
        options={{headerShown: false}}
      />
      <Stack.Screen
        name="SplashScreen"
        component={SplashScreen}
        options={{headerShown: false}}
      />

    </Stack.Navigator>
  );
};

export default Router;
