import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Container from '../Container';

const Form = (props) => (
  <Container>
    <View style={styles.titleContainer}>
      <Text style={{...styles.title, ...props.style}}>{props.title}</Text>
    </View>
    {props.children}
  </Container>
);

export default Form;

const styles = StyleSheet.create({
  titleContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 20,
  },
  title: {
    fontSize: 24,
  },
});
