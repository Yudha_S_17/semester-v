import React from 'react';
import {
  StyleSheet, Text, TouchableOpacity, View,
} from 'react-native';
import FAIcon from 'react-native-vector-icons/dist/FontAwesome'
import Iicon from 'react-native-vector-icons/dist/Ionicons'

const Button = (props) => (
  <TouchableOpacity onPress={props.onPress}>
    <View style={{ ...styles.container, ...props.style }}>
      <Text style={styles.text}>{props.name}</Text>
      {props.icon ? <Iicon name={props.icon} size={20} color={"white"} /> : null}
    </View>
  </TouchableOpacity>
);

export default Button;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: '#606060',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    margin: 5,
    color: '#fff',
    fontSize: 18,
    fontWeight: 'bold',
  },
});
