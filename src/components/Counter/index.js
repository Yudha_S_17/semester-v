import React, { useState, useEffect } from 'react';
import {
  Alert,
  SafeAreaView, StyleSheet, Text, View,
} from 'react-native';
import Button from '../Button';

const Counter = () => {
  const [number, setNumber] = useState(0);

  useEffect(() => {
    if (number > 5) {
      Alert.alert('number > 5');
    }
  }, [number]);

  return (
    <View style={styles.container}>
      <Text>{number}</Text>
      <Button
        name="Tambah"
        onPress={
        () => { setNumber(number + 1); }
     }
      />
    </View>
  );
};

export default Counter;

const styles = StyleSheet.create({
  container: {
    padding: 20,
  },
  title: {
    textAlign: 'center',
  },
});
