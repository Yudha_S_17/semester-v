import React from 'react';
import {
  Image, StyleSheet, Text, View,
} from 'react-native';
import colors from '../../styles/colors';
import Button from '../Button';

const Card = (props) => (
  <View style={styles.container}>
    <View style={styles.content}>
      <View style={styles.item}>
        <Image source={{ uri: 'https://placeimg.com/150/150/people' }} style={styles.image} />
        <View style={{ width: 170 }}>
          <Text style={styles.name}>{props.name}</Text>
          <Text>
            {props.subscriber}
            {' '}
            subscribers
          </Text>
        </View>
        <Button name="Vote" style={{ width: 80, backgroundColor: colors.primary }} />
      </View>
    </View>
  </View>
);

export default Card;

const styles = StyleSheet.create({
  container: {
    alignItems: 'center',
    padding: 10,
  },
  content: {
    width: '100%',
    shadowColor: 'black',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowRadius: 6,
    shadowOpacity: 0.26,
    elevation: 8,
    backgroundColor: 'white',
    padding: 10,
    borderRadius: 10,
  },
  item: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  image: {
    width: 80,
    height: 80,
    borderRadius: 50,
    marginRight: 15,
  },
  name: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});
