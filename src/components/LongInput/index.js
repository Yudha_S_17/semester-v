/* eslint-disable linebreak-style */
import React from 'react';
import {
  StyleSheet, Text, TextInput, View,
} from 'react-native';
import colors from '../../styles/colors';

const LongInput = (props) => (
  <View>
    {props.title ? <Text>{props.title}</Text> : null}
    <TextInput
      {...props}
      style={{...styles.input, ...props.style}}
      placeholder={props.title ? props.title : null}
    />
  </View>
);

export default LongInput;

const styles = StyleSheet.create({
  input: {
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    backgroundColor: colors.input.backgroundColor,
  },
});
