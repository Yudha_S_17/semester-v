import React from 'react';
import { StyleSheet, Text, View, Button } from 'react-native';
import colors from '../../styles/colors';
import Iicon from 'react-native-vector-icons/Ionicons';

const Header = (props, {onPress}) => (
  <View style={styles.container}>
    {props.icon ? <Iicon name= {props.icon} size={props.sizeIcon} color="white" /> : null }
    <Text style={{...styles.text, ...props.style}}>{props.name}</Text>
  </View>
);

export default Header;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    height: 60,
    backgroundColor: colors.third,
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    color: '#fff',
    fontSize: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
