import React from 'react';
import {
  StyleSheet, Text, TextInput, View,
} from 'react-native';
import colors from '../../styles/colors';

const Input = (props) => (
  <View>
    {props.title ? <Text>{props.title}</Text> : null}
    <TextInput
      {...props}
      style={{ ...styles.input, ...props.style }}
    />
  </View>
);

export default Input;

const styles = StyleSheet.create({
  input: {
    fontFamily: "MonteCarlo-Regular",
    height: 30,
    borderBottomWidth: 1,
    borderBottomColor: 'grey',
    backgroundColor: colors.input.backgroundColor,
  },
});
