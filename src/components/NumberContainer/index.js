import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import colors from '../../styles/colors';

const NumberContainer = (props) => (
  <View style={styles.container}>
    <Text style={styles.number}>{props.children}</Text>
  </View>
);

export default NumberContainer;

const styles = StyleSheet.create({
  container: {
    borderWidth: 2,
    borderColor: colors.primary,
    padding: 10,
    borderRadius: 5,
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  number: {
    fontSize: 22,
    fontWeight: 'bold',
  },
});
