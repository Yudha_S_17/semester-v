export default {
  first: '#fefefe',
  second: '#008000',
  third: '#FF7F00',
  input: {
    backgroundColor: '#fefefe',
  },
  button: {
    confirm: '#1e90ff',
    accept: '#036635',
    cancel: '#c3352e',
  },
};
