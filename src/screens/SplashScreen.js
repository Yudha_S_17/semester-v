/* eslint-disable linebreak-style */
import React, {useEffect} from 'react';
import {ActivityIndicator, Image, StatusBar, StyleSheet, Text, View} from 'react-native';
import colors from '../styles/colors';
import Gap from '../components/Gap';
import {APP_VERSION, APP_NAME} from '../config/index';

const SplashScreen = ({navigation}) => {
  useEffect(() => {
    setTimeout(() => {
      navigation.replace('Beranda');
    }, 3000);
  }, [navigation]);

  return (
    <View style={styles.container}>
      <StatusBar barStyle='dark-content' backgroundColor={colors.first} />
      <Image source={require('../assets/images/usu.png')} style={styles.image} />
      <Text style={styles.title}>Please Wait..</Text>
      <Gap height={30} />
      <ActivityIndicator color={colors.third} size='large' />
      <Gap height={150} />
      <Text style={{color: '#000'}}>
        {' '}
        {APP_NAME}
      </Text>
      <Text style={{color: '#000'}}>
                Version
        {' '}
        {APP_VERSION}
      </Text>
    </View>
  );
};

export default SplashScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fefefe',
    alignItems: 'center',
    justifyContent: 'center',
  },
  image: {
    width: 150,
    height: 150,
    resizeMode: 'contain',
    marginVertical: 30,
  },
  title: {
    fontSize: 18,
    color: '#000000',
    fontFamily: 'NanumGothic-Regular',
  },
});
