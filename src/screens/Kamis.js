/* eslint-disable linebreak-style */
import React from 'react';
import {StatusBar, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Container from '../components/Container';
import Header from '../components/Header';
import colors from '../styles/colors';
import Iicon from 'react-native-vector-icons/Ionicons';
import Gap from '../components/Gap';

const Kamis = ( {navigation} ) => {
  return (
    <View>
      <StatusBar barStyle='default' backgroundColor={colors.third} />
      <Header name="Jadwal Kuliah" style={styles.header} />
      <View>
        <Container>
          <View style={styles.kop}>
            <TouchableOpacity onPress={() => navigation.goBack()} >
              <Iicon name="chevron-back" size={20} />
            </TouchableOpacity>
            <Text style={styles.title}>Kamis</Text>
            <Text>
              {' '}
            </Text>
          </View>
          <Gap height={30} />
          <View style={styles.konten} >
            <View style={styles.jadwal}>
              <Text style={styles.isi}>Rekper</Text>
              <Text style={styles.isi}>07.30 - 09.10</Text>
            </View>
            <View style={styles.jadwal}>
              <Text style={styles.isi}>PSD</Text>
              <Text style={styles.isi}>09.10 - 11.40</Text>
            </View>
          </View>
        </Container>
      </View>
    </View>
  );
};

export default Kamis;

const styles = StyleSheet.create({
  header: {
    fontSize: 30,
    fontFamily: 'Lobster-Regular',
  },
  kop: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  title: {
    fontSize: 35,
    fontFamily: 'StyleScript-Regular',
  },
  konten: {
    flexDirection: 'column',
  },
  jadwal: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  isi: {
    fontSize: 20,
    fontFamily: 'Righteous-Regular',
  },
});
