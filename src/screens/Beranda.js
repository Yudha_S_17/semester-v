/* eslint-disable linebreak-style */
import React from 'react';
import {ScrollView, StatusBar, StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import Container from '../components/Container';
import Header from '../components/Header';
import colors from '../styles/colors';

const Beranda = ({navigation}) => {
  return (
    <View>
      <StatusBar barStyle='default' backgroundColor={colors.third} />
      <Header name="Beranda" style={styles.header} />
      <ScrollView style={styles.menu}>
        <TouchableOpacity onPress={() => navigation.navigate('senin')}>
          <Container>
            <Text style={styles.text}>Jadwal Kuliah</Text>
          </Container>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => navigation.navigate('Kalender')}>
          <Container>
            <Text style={styles.text}>Kalender Akademik</Text>
          </Container>
        </TouchableOpacity>
      </ScrollView>
    </View>
  );
};

export default Beranda;

const styles = StyleSheet.create({
  header: {
    fontSize: 30,
    fontFamily: 'Lobster-Regular',
  },
  menu: {
    flexDirection: 'column',
  },
  text: {
    fontSize: 30,
    fontFamily: 'BebasNeue-Regular',
  },
});
