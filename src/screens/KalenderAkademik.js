/* eslint-disable linebreak-style */
import React from 'react';
import {Image, StatusBar, StyleSheet, ScrollView, View} from 'react-native';
import Container from '../components/Container';
import Header from '../components/Header';
import colors from '../styles/colors';

const KalenderAkademik = () => {
  return (
    <View>
      <StatusBar barStyle='default' backgroundColor={colors.third} />
      <Header name="Kalender Akademik 2021/2022" style={styles.header}/>
      <ScrollView>
        <Image source={require('../assets/images/kalenderAkademik.jpeg')} style={styles.image} />
      </ScrollView>
    </View>
  );
};

export default KalenderAkademik;

const styles = StyleSheet.create({
  header: {
    fontSize: 25,
    fontFamily: 'Lobster-Regular',
  },
  image: {
    width: 400,
    height: 600,
    resizeMode: 'contain',
    marginTop: -50,
  },
});
